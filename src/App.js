import React, { Component } from 'react';
import './App.css';
import Personal from "./components/personal";
import Work from "./components/work";

class App extends Component {
  render() {
    return (
      <React-Fragment>
        <h1 id="title">Title</h1>
      <div className="App">
        <Personal/>
        <Work/> 
      </div>
      </React-Fragment>
    );
  }
}

export default App;
