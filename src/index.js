import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import {createStore,combineReducers} from "redux";
import {Provider} from "react-redux";
import {personalReducer} from "./reducers/personalReducer";
import {workReducer} from "./reducers/workReducer";
import {data} from "./components/constant";

const rootReducer=combineReducers({
    personal:personalReducer,
    work:workReducer
  });

const store=createStore(rootReducer,data,window.devToolsExtension && window.devToolsExtension());

ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root'));
registerServiceWorker();
