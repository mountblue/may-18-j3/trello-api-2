export const FETCH_PERSONAL_DATA="FETCH_PERSONAL_DATA";
export const FETCH_CHECKLIST_PERSONAL="FETCH_CHECKLIST_PERSONAL";
export const ADD_PERSONAL_ITEM="ADD_PERSONAL_ITEM";
export const DELETE_PERSONAL_ITEM="DELETE_PERSONAL_ITEM";
export const CHANGE_PERSONAL_STATE="CHANGE_PERSONAL_STATE";

export function fetchPersonalData(listData,cardData){
    return{
        type:FETCH_PERSONAL_DATA,
        payload:{
            listData:listData,
            cardData:cardData
        }
    }
}

export function fetchChecklist(checklist){
    return{
        type:FETCH_CHECKLIST_PERSONAL,
        payload:{
            checklistData:checklist
        }
    }
}