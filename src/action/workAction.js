export const FETCH_WORK_DATA="FETCH_WORK_DATA";
export const FETCH_CHECKLIST_WORK="FETCH_CHECKLIST_WORK";
export const ADD_WORK_ITEM="ADD_WORK_ITEM";
export const DELETE_WORK_ITEM="DELETE_WORK_ITEM";
export const CHANGE_WORK_STATE="CHANGE_WORK_STATE";

export function fetchWorkData(listData,cardData){
    return{
        type:FETCH_WORK_DATA,
        payload:{
           listData:listData,
            cardData:cardData
        }
    }
}

export function fetchChecklist(checklist){
    return{
        type:FETCH_CHECKLIST_WORK,
        payload:{
            checklistData:checklist
        }
    }
}