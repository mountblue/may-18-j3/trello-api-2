import {FETCH_WORK_DATA} from "../action/workAction";
import {FETCH_CHECKLIST_WORK} from "../action/workAction";
import {data} from "../components/constant";

export function workReducer(state=data.work,action){

    switch(action.type){
        case FETCH_WORK_DATA:
        console.log(action.payload)
            return {
                ...state,
                listData:action.payload.listData,
                cardData:action.payload.cardData,
            }
        case FETCH_CHECKLIST_WORK:
            return {
                ...state,
                checklistData: action.payload.checklistData
            }
        default: return state;
    }
}