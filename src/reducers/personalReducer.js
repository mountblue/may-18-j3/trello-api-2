import {FETCH_PERSONAL_DATA} from "../action/personalAction";
import {FETCH_CHECKLIST_PERSONAL} from "../action/personalAction";
import {data} from "../components/constant";

export function personalReducer(state=data.personal,action){

    switch(action.type){
        case FETCH_PERSONAL_DATA:
            console.log(action.payload)
            return {
                ...state,
                listData:action.payload.listData,
                cardData:action.payload.cardData,
            }
        case FETCH_CHECKLIST_PERSONAL:
            return {
                ...state,
                checklistData: action.payload.checklistData
            }
        default: return state;
    }
}