import React, { Component } from 'react';
import axios from "axios";
import Modal from "react-modal";
import Input from "./stateless/input";
import {connect} from "react-redux";
import {fetchPersonalData,fetchChecklist} from "../action/personalAction";
import {key,token} from "./constant";


Modal.setAppElement('#root')

class Personal extends Component {

  constructor(props){
    super(props);
    this.state={
        listData:{},
        cardData:[],
        checklistData:[],
        modalIsOpen:false,
        newItem:""
    }
    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }  

  openModal(e) {
    this.setState({modalIsOpen: true});
    axios.get("https://api.trello.com/1/cards/"+e.target.id+"/checklists?key="+key+"&token="+token)
    .then((res)=>{
        this.setState({
            checklistData:res.data
        })
    })
    .then(()=>{
        this.props.fetchChecklist(this.state.checklistData);
    })
    .catch((err)=>{
        console.error(err);
    })
  }

  closeModal() {
    this.setState({modalIsOpen: false});
  }

  componentWillMount(){
    axios.get("https://api.trello.com/1/boards/LwuWnvRS/lists?key="+key+"&token="+token)
    .then((res)=>{
        res.data.map((listItem)=>{
            if(listItem.name==="Personal")
            this.setState({
                listData:listItem
            })
            return null;
        })
    })
    .then(()=>{
        axios.get("https://api.trello.com/1/lists/"+this.state.listData.id+"/cards?key="+key+"&token="+token)
        .then((res)=>{          
            this.setState({
                cardData:res.data
            })
        })
        .then(()=>{
            console.log(this.state);
            this.props.fetchList(this.state.listData,this.state.cardData);
        })
        .catch(function(err){
            console.error(err);
        })
    })
    .catch(function(err){
        console.error(err);
    })
  } 

  handleChange(e){
    this.setState({
        newItem:e.target.value?e.target.value:""
    })
  }

  addItem(e){
      console.log()
    axios.post("https://api.trello.com/1/checklists/"+e.target.id+"/checkItems?name="+this.state.newItem+"&key="+key+"&token="+token)
    .then((res)=>{
        console.log(res)
        alert("Succefully Added");
    })
    .catch((err)=>{
        console.error(err);
    })
  }

  deleteItem(e){
    axios.delete("https://api.trello.com/1/checklists/"+e.target.name+"/checkItems/"+e.target.id+"?key="+key+"&token="+token)
    .then((res)=>{
        console.log(res)
        alert("Succefully Deleted");
    })
    .catch((err)=>{
        console.error(err);
    })
  }

  render() {
    return (
      <div className="Personal">
         <h1 key={this.props.personal.listData.id}>{this.props.personal.listData.name}</h1>
         <div className="Card">
            <ul>
            {
                this.props.personal.cardData.map((cardItem)=>{
                    return (
                        <li id={cardItem.id} key={cardItem.id} onClick={this.openModal}>{cardItem.name}</li>
                    )
                })
            }
            </ul>
         </div>
         <Modal isOpen={this.state.modalIsOpen}
          onRequestClose={this.closeModal}>
            <ul>
                {
                    this.props.personal.checklistData.map((checklistItem)=>{
                        return(
                            <React-Fragment>
                                <h4>{checklistItem.name}</h4>
                                    <Input place="Enter New Item to Add" onChange={this.handleChange.bind(this)}/><br/>
                                    <button id={checklistItem.id} onClick={this.addItem.bind(this)}>Add</button><br/><br/>
                                    {
                                        checklistItem.checkItems.map((checkItem)=>{
                                            return <li key={checkItem.id}>{checkItem.name} which is {checkItem.state}&nbsp;<button id={checkItem.id} name={checklistItem.id} onClick={this.deleteItem.bind(this)}>delete</button></li>
                                        })
                                    }
                            </React-Fragment>
                        )
                    })
                }
            </ul>
            <button onClick={this.closeModal}>x</button>
         </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => ({
    personal:state.personal
  });
  
  const mapActionsToProps = {
    fetchList:fetchPersonalData,
    fetchChecklist:fetchChecklist
  };

export default connect(mapStateToProps,mapActionsToProps)(Personal);
