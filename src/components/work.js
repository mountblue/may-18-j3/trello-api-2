import React, { Component } from 'react';
import axios from "axios";
import Modal from "react-modal";
import Input from "./stateless/input";
import {key,token} from "./constant";
import {connect} from "react-redux";
import {fetchWorkData,fetchChecklist} from "../action/workAction";

Modal.setAppElement('#root')

class Work extends Component {

    constructor(props){
        super(props);
        this.state={
            listData:{},
            cardData:[],
            checklistData:[],
            modalIsOpen:false,
            newItem:""
        }
        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
      }  
    
  handleChange(e){
    this.setState({
        newItem:e.target.value?e.target.value:""
    })
  }

      openModal(e) {
        this.setState({modalIsOpen: true});
        axios.get("https://api.trello.com/1/cards/"+e.target.id+"/checklists?key="+key+"&token="+token)
        .then((res)=>{
            this.setState({
                checklistData:res.data
            })
        })
        .then(()=>{
            this.props.fetchChecklist(this.state.checklistData);
        })
      }
    
      closeModal() {
        this.setState({modalIsOpen: false});
      }
    
      componentWillMount(){
        axios.get("https://api.trello.com/1/boards/LwuWnvRS/lists?key="+key+"&token="+token)
        .then((res)=>{
            res.data.map((listItem)=>{
                if(listItem.name==="Work")
                this.setState({
                    listData:listItem
                })
                return null;
            })
        })
        .then(()=>{
            axios.get("https://api.trello.com/1/lists/"+this.state.listData.id+"/cards?key="+key+"&token="+token)
        .then((res)=>{          
            this.setState({
                cardData:res.data
            })
        })
        .then(()=>{
            console.log(this.state);
            this.props.fetchList(this.state.listData,this.state.cardData);
        })
        .catch(function(err){
            console.error(err);
        })
        })
        .catch(function(err){
            console.error(err);
        })
      } 

      addItem(e){
        console.log()
      axios.post("https://api.trello.com/1/checklists/"+e.target.id+"/checkItems?name="+this.state.newItem+"&key="+key+"&token="+token)
      .then((res)=>{
          alert("Succefully Added");
          this.forceUpdate();
      })
      .catch((err)=>{
          console.error(err);
      })
    }
      deleteItem(e){
        axios.delete("https://api.trello.com/1/checklists/"+e.target.name+"/checkItems/"+e.target.id+"?key="+key+"&token="+token)
        .then((res)=>{
            alert("Succefully Deleted");
            this.forceUpdate();
        })
        .catch((err)=>{
            console.error(err);
        })
      }

      render() {
        return (
          <div className="Work">
             <h1 key={this.props.work.listData.id}>{this.props.work.listData.name}</h1>
             <div className="Card">
                <ul>
                {
                    this.props.work.cardData.map((cardItem)=>{
                        return (
                            <li id={cardItem.id} key={cardItem.id} onClick={this.openModal}>{cardItem.name}</li>
                        )
                    })
                }
                </ul>
             </div>
             <Modal isOpen={this.state.modalIsOpen}
              onRequestClose={this.closeModal}>
                <ul>
                    {
                        this.props.work.checklistData.map((checklistItem)=>{
                            return(
                                <React-Fragment>
                                    <h4>{checklistItem.name}</h4>
                                    <Input place="Enter New Item to Add" onChange={this.handleChange.bind(this)}/><br/>
                                    <button id={checklistItem.id} onClick={this.addItem.bind(this)}>Add</button><br/><br/>
                                    {
                                        checklistItem.checkItems.map((checkItem)=>{
                                            return <li key={checkItem.id}>{checkItem.name} which is {checkItem.state}&nbsp;<button id={checkItem.id} name={checklistItem.id} onClick={this.deleteItem.bind(this)}>delete</button></li>
                                        })
                                    }
                                </React-Fragment>
                            )
                        })
                    }
                </ul>
                <button onClick={this.closeModal}>x</button>
             </Modal>
          </div>
        );
      }
    }
    
    const mapStateToProps = state => ({
        work:state.work
      });
      
      const mapActionsToProps = {
        fetchList:fetchWorkData,
        fetchChecklist:fetchChecklist
      };
    
    export default connect(mapStateToProps,mapActionsToProps)(Work);
    