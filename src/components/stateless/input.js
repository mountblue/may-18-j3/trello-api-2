import React from "react";

function Input(props){
  return(
    <React.Fragment>
      <input type="text" value={props.value} style={props.style} placeholder={props.place} name={props.name} onChange={props.onChange} id={props.id} className={props.class}/>
      <br />
    </React.Fragment>
  );
}

export default Input;
